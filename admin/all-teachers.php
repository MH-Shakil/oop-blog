<?php 
    include "../db/connection.php";
    include "pages/admin-header.php";
    include '../classes/admin-class.php';
    include 'pages/all-contents.php';
    $teachers  = new teachers();
    if (isset($_GET['dtid'])) {
      $teachers->deleteTeachers($_GET['dtid']);
    }
 ?>
 <div class="row">
   <div class="col-md-8" style="padding: 0px;margin:0px;"><h2>Teachers List </h2></div>
   <div class="col-md-4 "><a href="teachers.php" class="btn btn-info mb-2 mt-4" style="width: 300px; float: right;">Add teachers</a></div>
 </div>
    <table class="table table-hover" align="center">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Section</th>
          <th>Mobile</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $select = "SELECT * FROM teachers ORDER BY id DESC";
          $data = $teachers->showTeachers($select);
         foreach ($data as $key => $teachersData) {
           echo '<tr>
                  <td>'.$teachersData['id'].'</td>
                  <td>'.$teachersData['name'].'</td>
                  <td>'.$teachersData['section'].'</td>
                  <td>'.$teachersData['mobile'].'</td>
                  <td>
                      <a href="?dtid='.$teachersData['id'].'" class="btn btn-warning">Delete</a>
                      <a href="edit-teachers.php?etid='.$teachersData['id'].'" class="btn btn-info">Info & Edit</a>
                    </td>
                  </td>
                </tr>';
              
          }

        ?>

      </tbody>
    </table>

  <?php include "pages/admin-footer.php"; ?>
    