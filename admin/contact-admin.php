<?php 
    session_start();
    if (!isset($_SESSION['status']) && !isset($_SESSION['id'])) {
      header('location:../login.php');
    }
    include "../db/connection.php";
    include '../classes/user-class.php';
    include "pages/admin-header.php";
    include 'pages/all-contents.php';

  $message = new message();
  $users = new users();
  $s_id = $_SESSION['id'];
  if (isset($_POST['submit']) && $_SERVER['REQUEST_METHOD']=="POST") {
    $r_id = $_GET['id'];
    $post = $message->replymessage($_POST,$_FILES,$r_id,$s_id);
  }

 ?>
 <div class="row">

   <div class="col-md-4 mt-2" style="background:  #45B39D; min-height: 380px;color:#fff;padding: 10px; max-width: 340px;margin: 0px">
    <h4 class="card mb-2" style="background: #fff; color:#000; padding: 8px;margin: 0px; ">Chat list...</h4>
     <?php 
        $select = "SELECT * FROM contact ORDER BY id DESC";
        $data = $message->showMessage($select);
        $list_array = [];
        foreach ($data as $key => $value) {
          if (in_array($value['sender_id'], $list_array) !=true) {
              array_push($list_array, $value['sender_id']);

               if ($value['sender_id']==0) {
                  $user = "SELECT * FROM users WHERE id=".$value['receiver_id'];
                  $info = $users->user_profile($user);
                  $info = mysqli_fetch_assoc($info);
                  
                  echo '<a 
                          href="?id='.$value['receiver_id'].'"
                          style="text-decoration:none;">
                            <div 
                              style="min-height:0px;">
                                <img 
                                  class="rounded-circle"
                                  style="float:left;" 
                                         width="40" 
                                         height="40px" 
                                         src="../upload/try.jpg"/>';
                  echo '<span 
                          style="color:#fff;">
                        '.$info['name'].'
                        </span>
                        <br>';
                  

              echo '<span 
                        style=" font-weight:bold;
                                color:#000; 
                                font-size:20px;">
                          '.$value['contact'].'
                    </span>
                    <br>';

               }elseif($value['receiver_id']==0){

                  $user = "SELECT * FROM users WHERE id=".$value['sender_id'];
                  $info = $users->user_profile($user);
                  $info = mysqli_fetch_assoc($info);

                  echo '<a  
                            href="?id='.$value['sender_id'].'"
                            style="text-decoration:none;">
                            <div style="min-height:0px;">
                              <img  class="rounded-circle"
                                    style="float:left;"
                                    width="40" 
                                    height="40px" 
                                    src="../upload/try.jpg"/>';

                  echo '<span 
                            style="color:#fff;">
                          '.$info['name'].'
                        </span>
                        <br>';
                  
                  echo '<span 
                            style="
                                  font-weight:bold;
                                  color:#000; 
                                  font-size:20px;">
                               '.$value['contact'].'
                        </span>
                        <br>';
                  }
                 echo '</div>
                      </a>
                      <hr>';
              }
            }


     ?>
   </div>
   <div class="col-md-8">
     <div class="card" style="margin:10px;">
  <div class="card-header mb-4" style="background: #45B39D;"><h3 style="padding: 0px;margin: 0px; color: #fff;">Write your Message</h3></div>
              <?php 
              if (isset($_GET['id'])) {
                
             
                  $user = "SELECT * FROM users WHERE id=".$_SESSION['id'];
                  $user =  $users->user_profile($user);
                  $user = mysqli_fetch_assoc($user);
                  $sender = $_SESSION['id'];
                  $receiver = $_GET['id'];
                  $select = " SELECT * FROM contact 
                              WHERE 
                              sender_id='$sender' AND receiver_id='$receiver' 
                              OR 
                              sender_id='$receiver' AND receiver_id='$sender'" ;

                  $data = $message->showMessage($select);
                  foreach ($data as $key => $value) {
                    if ($value['sender_id']==$_SESSION['id']) {
                      echo '<div 
                                style="min-height:60px; 
                                float:right;">
                                <img 
                                  style="float:right;
                                      border-radius:20px 20px 20px 20px;" 
                                  width="40" 
                                  height="40px" 
                                  src="../upload/try.jpg"/>';
                      echo '<div 
                                class="card mb-1"
                                style="
                                      border-radius:15px 0px 15px 15px;
                                      color:#fff;
                                      background:#45B39D;
                                      margin-right:5px;
                                      margin-top:10px;
                                      float:right;
                                      text-align:justify; 
                                      min-width:45px;
                                      max-width:450px;">';

                      echo '<p  
                              style="
                                    padding:0px 5px;
                                    margin-top:6px;
                                    margin-bottom:5px;">
                                '.$value['contact'].'
                            </p>';

                      echo '</div>
                          </div>';

                    }else{
                        echo '<div 
                                style="min-height:60px; 
                                float:left;">
                                <img 
                                  style="float:left;
                                      border-radius:20px 20px 20px 20px;" 
                                  width="40" 
                                  height="40px" 
                                  src="../upload/try.jpg"/>';
                      echo '<div 
                                class="card mb-1"
                                style="
                                      border-radius:0px 15px 15px 15px;
                                      color:#fff;
                                      background:#4Ab37D;
                                      margin-left:5px;
                                      margin-top:10px;
                                      float:left;
                                      text-align:justify; 
                                      min-width:45px;
                                      max-width:450px;">';

                      echo '<p  
                              style="
                                    padding:0px 5px;
                                    margin-top:6px;
                                    margin-bottom:5px;">
                                '.$value['contact'].'
                            </p>';

                      echo '</div>
                          </div>';
                      
                    }
                  }
               }
                ?>
  <div class="card-body">
      <form action=""
            method="post" 
            enctype="multipart/form-data">
            <div class="form-group">
              <h3>Message Us</h3>
              <textarea class="form-control"
                        rows="3" 
                        name="message">
              </textarea>
              <input type="file"
                     name="file" 
                     class="mt-2">
            </div>
          <button type="submit"
                 class="btn btn-primary" 
                 style="width: 100px;" 
                 name="submit">
              Message
          </button>
      </form>
    </div>
  </div>
</div>
</div>


  <?php include "pages/admin-footer.php"; ?>
