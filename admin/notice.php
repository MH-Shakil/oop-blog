<?php 
    session_start();
    if (!isset($_SESSION['status']) && !isset($_SESSION['id'])) {
      header('location:../login.php');
    }
    include "../db/connection.php";
    include '../classes/admin-class.php';
    include "pages/admin-header.php";
    include 'pages/all-contents.php';

  $notice = new notice();
  if (isset($_POST['submit']) && $_SERVER['REQUEST_METHOD']=="POST") {
    $notice = $notice->createNotice($_POST,$_FILES);
    if ($notice) {
      header("location:viewnotice.php");
    }else{
      echo "data not inserted";
    }
  }

 ?>
 <div class="row">
  <div class="col-md-2 mt-2">
    <a class="btn btn-info" href="viewnotice.php">Manage Notice</a>
  </div>
    <div class="col-md-9">
      <div class="card" style="">
        <div class="card-header" style="background: #45B39D;"><h3 style="padding: 0px;margin: 0px; color: #fff;">Write a notice</h3></div>
          <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <h3>Title.</h3>
                <input class="form-control mb-2" type="text" name="title" style="border-radius: 50px 50px 50px 50px;border: none;" placeholder="Enter a post title">
                <h3>Details.</h3>
                <textarea class="form-control" rows="3" name="details" placeholder="Write a notice"></textarea>
                <input type="file" name="file" class="mt-2">
              </div>
         <button type="submit" class="btn btn-primary" style="width: 100px;" name="submit">Submit</button>
      </form>
      </div>
    </div>
  </div>
</div>