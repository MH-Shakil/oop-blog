<?php
     include "../db/connection.php";
     include "pages/admin-header.php";
     include '../classes/admin-class.php';

     ?>
  <div id="wrapper">
    <?php include "pages/admin-side-nav.php"; ?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include "pages/admin-topbar.php"; ?>

        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Student</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">219</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Earnings (Annual)</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tasks</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                        </div>
                        <div class="col">
                          <div class="progress progress-sm mr-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Requests</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->

         <?php 
  //include 'admin-classes/classes.php';
 ?>
        <?php 
          $std  = new students();
          if (isset($_GET['Isid'])) {
          $data = $std->stdUpdt($_GET['Isid']);
          }
         foreach ($data as $key => $stdData) {
          echo '<h2 style="margin-left: 380px;">Add Students</h2>
            <hr>
            <div class="row">
              <div class="col-md-4">
                <a href="students.php" class="btn btn-info mb-2 mt-4" style="width: 300px;">Viwe Stundents</a>
              </div>
              <div class="col-md-8">
                <form method="post" action="" enctype="multipart/form-data">
                  <div class="form-row">
                      <input type="hidden" value="'.$stdData['id'].'" name="id">
                    <div class="form-group col-md-7">
                      <label for="inputCity">Name</label>
                      <input type="text" value="'.$stdData['name'].'" name="name" class="form-control" id="inputCity">
                    </div>
                    <div class="form-group col-md-8">
                      <label for="inputState">Class</label>
                      <select id="inputState" class="form-control" name="class">
                        <option selected value="'.$stdData['class'].'">'.$stdData['class'].'</option>
                        <option  value="4">Four</option>
                        <option value="5">Five</option>
                        <option value="6">Six</option>
                        <option value="7">Seven</option>
                        <option value="8">Eight</option>
                        <option value="9">Nine</option>
                        <option value="10">Ten</option>
                        <option value="11">Inter(1st)</option>
                        <option value="12">Inter(2nd)</option>
                      </select>
                      
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputState">Group</label>
                      <select id="inputState" class="form-control" name="group">
                        <option selected value="'.$stdData['section'].'">'.$stdData['section'].'</option>
                        <option value="General">General</option>
                        <option value="Scince">Scince</option>
                        <option value="Arts">Arts</option>
                        <option value="Commerce">Commerce</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mobile number</label>
                    <input type="number" name="mobile" value="'.$stdData['mobile'].'" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <label for="inputCity">Address</label>
                      <input type="text" name="address" value="'.$stdData['address'].'" class="form-control" id="inputCity">
                    </div>
                  </div>
                  <div class="row">
                  <div class="form-group col-md-4">
                      <label for="inputCity">Gender</label>
                      <select id="inputState" class="form-control" name="gender">
                        <option selected value="'.$stdData['gender'].'">'.$stdData['gender'].'</option>
                        <option  value="male">Male</option>
                        <option value="female">Female</option>
                      </select>
                  </div>
                  <div class="form-group col-md-8">
                       <label for="inputCity" style="margin-left: 15px;">Image</label><br>
                       <img height="200px" width="150" src="upload/'.$stdData['image'].'">
                      <input type="file" name="file" value="'.$stdData['image'].'" class="form-control btn btn-outline-light" id="inputCity">
                  </div>
                </div>
                  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>
            </div> ';

              
          } 
          if (isset($_POST['submit']) && $_SERVER['REQUEST_METHOD']=="POST" ) {
              $data = $std->updatestd($_POST,$_FILES);
          }

          ?>





  <?php include "pages/admin-footer.php"; ?>