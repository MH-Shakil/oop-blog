<?php 
    include "../db/connection.php";
    include "pages/admin-header.php";
    include '../classes/admin-class.php';
    include 'pages/all-contents.php';
  $std  = new students();
  if (isset($_GET['Isid'])) {
      $data = $std->stdDlt($_GET['Isid']);
    }
 ?>
        <div class="row">
              <a  
                href="add-students.php" 
                class="btn btn-info mr-2 mb-4 ml-2" 
                style="color:#fff;"
              >Add Student</a>
              <a  
                href="" 
                class="btn btn-info mr-2 mb-4 " 
                style="color:#fff;"
              >Manage students</a>
              <a  
                href="" 
                class="btn btn-info mr-2 mb-4 " 
                style="color:#fff;"
              >Students Class</a>
          </div>
        <div class="row">
          
          <?php
         $data = $std->stdShow();
          foreach ($data as $key => $stdData) { 
            echo '<div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="row" style="padding:2px 15px;">
                  <div class="col-md-4" style="padding:0px;">';
                  if ($stdData['image']==NULL && $stdData['gender']=='female') {
                      echo ' <img class="rounded-circle" width="65" height="90px" src="upload/female.png"/>';
                  }elseif ($stdData['image']==NULL && $stdData['gender']=='male') {
                      echo ' <img class="rounded-circle" width="65" height="90px" src="upload/male.png"/>';
                  }else{
                       echo ' <img class="rounded-circle" width="65" height="90px" src="upload/'.$stdData['image'].'"/>';
                  }
                 
            echo '
             
                </div>
                  <div class="col-md-8">
                    <h5 style="font-weight:bold;padding:0px;margin:0px; margin-left:0px;">'.$stdData['name'].'</h5>
                    <small style="padding:0px;margin:0px; margin-left:0px;">Class : '.$stdData['class'].'</small><br>
                    <small style="padding:0px;margin:0px; margin-left:0px;">Id : '.$stdData['id'].'</small>
                      
                  </div>
                </div>
              <div class="row" style="">
                  <a href="stdDetails.php?Isid='.$stdData['id'].'" style="margin-left:30px;float:left;">Edit</a>
                  <a href="?Isid='.$stdData['id'].'" style="float:right;margin-left:140px;">Delete</a>
              </div>
              </div>
            </div>';
          }
          ?>
        </div>
  <?php include "pages/admin-footer.php"; ?>
