<?php 
    session_start();
    if (!isset($_SESSION['status']) && !isset($_SESSION['id'])) {
      header('location:../login.php');
    }
    include "../db/connection.php";
    include '../classes/admin-class.php';
    include "pages/admin-header.php";
    include 'pages/all-contents.php';

  $notice = new notice();
  if (isset($_GET['notice_id'])) {
    $delete = "DELETE FROM notice WHERE id=".$_GET['notice_id'];
    $delete = $notice->viewNotice($delete);
  }
 ?>
 <div class="row">
  <div class="col-md-2 mt-2">
    <a class="btn btn-info" href="notice.php">Add Notice</a>
  </div>
    <div class="col-md-9">
      <?php 
          $select = "SELECT * FROM notice";
          $data = $notice->viewNotice($select);
          foreach($data as $key => $info) {
            echo '<div class="card mb-3" style="text-align:center;">
                          <div class="card-header" 
                               style="background: #45B39D;">
                               <div class="row">
                                    <div class="col-md-2">
                                        <a href="editnotice.php?id='.$info['id'].'" class="btn btn-denger" style="color:#fff; margin-right:50px;">Edit</a>
                                    </div>
                                    <div class="col-md-8">
                                    <h5 style=" padding: 0px;
                                          margin: 0px; 
                                          color: #fff;
                                          text-align:center;
                                          margin-top:10px;">
                                         Noticed on Novenbar 8, 2020                             
                                    </h5></div>
                                  
                                    <div class="col-md-1">
                                    <a href="?notice_id='.$info['id'].'" class="btn btn-denger" style=" color:#fff; margin-left:50px;">Delete</a></div>
                                </div>
                          </div>
                          <div class="card-body">
                              <h4 style="margin-bottom:0px;">'.$info['title'].'</h4>
                              ________________________________________________________________________
                              <h6>'.$info['details'].'</h6>
                      </div>
                    </div>';
          }

       ?>
  </div>
</div>