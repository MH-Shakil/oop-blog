<?php 
    include "../db/connection.php";
    include "pages/admin-header.php";
    include '../classes/admin-class.php';
    include 'pages/all-contents.php';
  $std  = new students();
  
  if (isset($_GET['dsid'])) {
    $std->stdDlt($_GET['dsid']);
  }

 ?>
  <div class="row">
     <div class="col-md-8" style="padding: 0px;margin:0px;"><h2>Students List </h2></div>
     <div class="col-md-4 "><a href="add-students.php" class="btn btn-info mb-2 mt-4" style="width: 300px; float: right;">Add Student</a></div>
  </div>
    <table class="table table-hover" align="center">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Class</th>
          <th>Group</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php 
         $data = $std->stdShow();
         foreach ($data as $key => $stdData) {
           echo '<tr>
                  <td>'.$stdData['id'].'</td>
                  <td>'.$stdData['name'].'</td>
                  <td>'.$stdData['class'].'</td>
                  <td>'.$stdData['section'].'</td>
                  <td>
                      <a href="?dsid='.$stdData['id'].'" class="btn btn-warning">Delete</a>
                      <a href="stdDetails.php?Isid='.$stdData['id'].'" class="btn btn-info">Info & Edit</a>
                    </td>
                  </td>
                </tr>';
              
          } ?>

      </tbody>
    </table>

  <?php include "pages/admin-footer.php";?>
    