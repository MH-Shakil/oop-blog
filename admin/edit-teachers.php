<?php 
    include "../db/connection.php";
    include "pages/admin-header.php";
    include '../classes/admin-class.php';
    include 'pages/all-contents.php';
    $teachers  = new teachers();
    if (isset($_POST['submit']) && $_SERVER['REQUEST_METHOD']=='POST') {
     $teachers->editTeachers($_POST);
    }
 ?>
 <div class="row">
   <div class="col-md-8" style="padding: 0px;margin:0px;"><h2>Teachers List </h2></div>
   <div class="col-md-4 "><a href="teachers.php" class="btn btn-info mb-2 mt-4" style="width: 300px; float: right;">Add teachers</a></div>
 </div>

    <table class="table table-hover" align="center">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Group</th>
          <th>Mobile</th>
          <th>Address</th>
          <th>Created_at</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          $select = "SELECT * FROM teachers WHERE id=".$_GET['etid'];
          $data = $teachers->showTeachers($select);
         foreach ($data as $key => $teachersData) {
           echo '<tr>
                  <td>'.$teachersData['id'].'</td>
                  <td>'.$teachersData['name'].'</td>
                  <td>'.$teachersData['section'].'</td>
                  <td>'.$teachersData['mobile'].'</td>
                  <td>'.$teachersData['address'].'</td>
                  <td>'.$teachersData['created_at'].'</td>
                </tr>
            </tbody>
          </table>';

      echo '<h2>Edit Teachers</h2>
        <div class="col-md-8">
              <form method="post" action="">
                <div class="form-row">
                  <div class="form-group col-md-8">
                  <input type="hidden" name="id" class="form-control" id="inputCity" value="'.$teachersData['id'].'">
                    <label for="inputCity">Name</label>
                    <input type="text" name="name" class="form-control" id="inputCity" value="'.$teachersData['name'].'">
                  </div>
                  <div class="form-group col-md-4">
                    <label for="inputState">Group</label>
                    <select id="inputState" class="form-control" name="group">
                      <option selected value="General">General</option>
                      <option value="Scince">Scince</option>
                      <option value="Arts">Arts</option>
                      <option value="Commerce">Commerce</option>
                    </select>
                  </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Mobile number</label>
                  <input type="number" name="mobile" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="'.$teachersData['mobile'].'">
                </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label for="inputCity">Address</label>
                    <input type="text" name="address" class="form-control" id="inputCity" value="'.$teachersData['address'].'">
                  </div>
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>';
              
          }

        ?>

  <?php include "pages/admin-footer.php"; ?>
