<?php 
    include "../db/connection.php";
    include "pages/admin-header.php";
    include '../classes/admin-class.php';
    include 'pages/all-contents.php';
  $std  = new students();
  if (isset($_POST['submit']) && $_SERVER['REQUEST_METHOD']=="POST") {
    $result = $std->studentsInsert($_POST,$_FILES);
    if ($result) {
      header('location:all-students.php');
    }
  }
 ?>
<h2 style="margin-left: 380px;">Add Students</h2>
<hr>
<div class="row">
  <div class="col-md-4">
    <a href="students.php" class="btn btn-info mb-2 mt-4" style="width: 300px;">Viwe Stundents</a>
  </div>
  <div class="col-md-8">
    <form method="post" action="" enctype="multipart/form-data">
      <div class="form-row">
        <div class="form-group col-md-7">
          <label for="inputCity">Name</label>
          <input type="text" name="name" class="form-control" id="inputCity">
        </div>
        <div class="form-group col-md-8">
          <label for="inputState">Class</label>
          <select id="inputState" class="form-control" name="class">
            <option selected value="4">Four</option>
            <option value="5">Five</option>
            <option value="6">Six</option>
            <option value="7">Seven</option>
            <option value="8">Eight</option>
            <option value="9">Nine</option>
            <option value="10">Ten</option>
            <option value="11">Inter(1st)</option>
            <option value="12">Inter(2nd)</option>
          </select>
          
        </div>
        <div class="form-group col-md-4">
          <label for="inputState">Group</label>
          <select id="inputState" class="form-control" name="group">
            <option selected value="General">General</option>
            <option value="Scince">Scince</option>
            <option value="Arts">Arts</option>
            <option value="Commerce">Commerce</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Mobile number</label>
        <input type="number" name="mobile" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-row">
        <div class="form-group col-md-12">
          <label for="inputCity">Address</label>
          <input type="text" name="address" class="form-control" id="inputCity">
        </div>
      </div>
      <div class="row">
      <div class="form-group col-md-4">
          <label for="inputCity">Gender</label>
          <select id="inputState" class="form-control" name="gender">
            <option selected value="male">Male</option>
            <option value="female">Female</option>
          </select>
      </div>
      <div class="form-group col-md-8">
           <label for="inputCity" style="margin-left: 15px;">Image</label>
          <input type="file" name="file" class="form-control btn btn-outline-light" id="inputCity">
      </div>
    </div>
      <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>

  <?php include "pages/admin-footer.php"; ?>
