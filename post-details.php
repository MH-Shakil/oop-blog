<?php
  session_start();
  if (!isset($_SESSION['id']) && !isset($_SESSION['name'])) {
    header('location:index.php');
  }
  include'db/connection.php';
  include"pages/header.php";
  include"pages/nav.php";
  include"classes/user-class.php";
  $post = new post();
  $std = new students();
  if (isset($_POST['comment_submit']) && $_SERVER['REQUEST_METHOD']=='POST') {
  $comment = $post->comment_insert($_POST,$_GET['post_id'],$_SESSION['id']);
  }
?>

  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">

    <?php 
    
    $id = $_GET['post_id'];
    $select = "SELECT * FROM post WHERE id=$id";
    $show = $post->showPost($select);
    foreach ($show as $key => $data) {
      $select = "SELECT * FROM users WHERE id=".$data['poster_id'];
      $poster = $post->postBy($select);
      $poster = mysqli_fetch_assoc($poster);
      $poster = $poster['name'];
        if ($data['image']==NULL) {
              echo '<img class="card-img-top mt-4" src="http://placehold.it/750x300" alt="Card image cap">';
            }else{
              echo '<img style="max-height:300px;" class="card-img-top mt-4" src="upload/post/'.$data['image'].'" alt="Card image cap">';
              }
        
            echo '<h1 class="mt-4">'.$data['title'].'</h1>

            <p class="lead">
              by';
              
                echo '<a href="#">'.$poster.'</a>';
              
              echo ' </p>

            <hr>

            <p>Posted on January 1, 2019 at 12:00 PM</p>

            <hr>


            <hr>
            
            <p class="lead">'.$data['post'].'</p>

            <hr>';
          
          }
    ?>
        <!-- Comments Form -->
        <div class="card my-4">
          <h5 class="card-header">Leave a Comment:</h5>
          <div class="card-body">
            <form class="" method="POST" action="">
              <div class="form-group">
                <textarea class="form-control" rows="3" name="comment"></textarea>
              </div>
              <button type="submit" class="btn btn-primary" name="comment_submit">Submit</button>
            </form>
          </div>
        </div>
        <?php 
          $select = "SELECT * FROM post_comment WHERE id='$id'  ORDER BY id DESC";
          $comment_show = $post->comment_show($select);
          foreach ($comment_show as $key => $value) {
            $select = "SELECT * FROM users WHERE id=".$value['commenter_id'];
            $commenter = $post->comment_show($select);
            $commenter = mysqli_fetch_assoc($commenter);
            
          
          
        ?>
        <!-- Comment with nested comments -->
        <div class="media mb-4">
          <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
          <div class="media-body">
           
            <h5 class="mt-0"><?php  echo $commenter['name'];?></h5>
            <?php echo $value['comments'];?>

          </div>
        </div>
        <?php } ?>

      </div>

      <!-- Sidebar Widgets Column -->
      <?php include "post/right-post.php";?>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
