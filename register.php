<?php 
session_start();
    include "db/connection.php";
    include "pages/header.php";
    include "pages/nav.php";
    include 'classes/user-class.php';
    $reg = new Allclasses();
    $std = new students();
    if ($_SERVER['REQUEST_METHOD']=="POST" && isset($_POST['submit'])) {
      $done = $std->studentsInsert($_POST,$_FILES);
    }
    if(isset($done)){
      //echo $done;
    }
      ?>
    
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Student Register</title>

  <!-- Custom fonts for this template-->
  <link href="admin/assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="admin/assets/css/sb-admin-2.min.css" rel="stylesheet">

</head>


  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block" style="background: blue;">
            <h1 style="
                      margin-left: 80px;
                      margin-top: 100px;
                      font-weight: bold;
                      color: #fff;
                      ">
                    SUCCESS TUTORIAL</h1>
          </div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Student Registraion!</h1>
              </div>

              <form class="user" method="POST" action="" enctype="multipart/form-data">
                <div class="form-group">
                  Name
                  <input type="text" name="name" class="form-control form-control-user" id="exampleFirstName" placeholder="Name" >
                </div>


                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    
                      Class<select id="inputState" class="form-control"style="height: 50px;border-radius: 50px 50px 50px 50px" name="class">
                        <option selected value="4" style="color: #000;">Four</option>
                        <option value="5">Five</option>
                        <option value="6">Six</option>
                        <option value="7">Seven</option>
                        <option value="8">Eight</option>
                        <option value="9">Nine</option>
                        <option value="10">Ten</option>
                        <option value="11">Inter(1st)</option>
                        <option value="12">Inter(2nd)</option>
                      </select>
                  
                  </div>

                  <div class="col-sm-6" style="color: #000;">
                    Group
                    <select style="height: 50px;border-radius: 50px 50px 50px 50px" id="inputState" class="form-control" name="group">
                      <option selected value="General">General</option>
                      <option value="Scince">Scince</option>
                      <option value="Arts">Arts</option>
                      <option value="Commerce">Commerce</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  Number
                  <input type="text" class="form-control form-control-user" name="mobile" placeholder="Enter your mobile Number">
                </div>
                <div class="form-group">
                  Address
                  <input type="text" class="form-control form-control-user" name="address" placeholder="Enter your address">
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        Gender
                        <select style="height: 50px;border-radius: 50px 50px 50px 50px" id="inputState" class="form-control" name="gender">
                          <option selected value="male">Male</option>
                          <option value="female">Female</option>
                        </select>
                    </div>
                    <div class="form-group col-md-8">
                         Image
                        <input type="file" name="file" class="form-control btn btn-outline-light" id="inputCity">
                    </div>
                  </div>
                <input type="submit" class="btn btn-primary btn-user btn-block" name="submit">
                
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="forgot-password.html">Forgot Password?</a>
              </div>
              <div class="text-center">
                <a class="small" href="login.html">Already have an account? Login!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="admin/assets/vendor/jquery/jquery.min.js"></script>
  <script src="admin/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="admin/assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="admin/assets/js/sb-admin-2.min.js"></script>

</body>

</html>
