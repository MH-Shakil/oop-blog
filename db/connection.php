<?php 
  class database{
    public $host="localhost";
    public $user="root";
    public $pass="";
    public $db="st";
    public $link;

    public function __construct(){
      $this->dbConnect();
    }
    public function dbConnect(){
        $this->link = mysqli_connect($this->host,$this->user,$this->pass,$this->db);
        return $this->link;
    }
    public function selectQuery($query)
    {
      return $this->link->query($query);
    }
  }
 ?>