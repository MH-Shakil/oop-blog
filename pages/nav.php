  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Success Tutorial</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <!-- start without login user can browse this page-->
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="register.php">Registraion</a>
          </li>
          
           <li class="nav-item">
            <a class="nav-link" href="notice.php">Notice</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Result</a>
          </li>
          <!-- end without login user can browse this page-->
          <?php 
            if (isset($_SESSION['id']) && $_SESSION['status']=='0') {
              echo '
              <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact</a>
              </li>
              <li class="nav-item">
            <a class="nav-link" href="profile.php">Profile</a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" href="post.php">post</a>
          </li>';
            }
           ?>
          
          
         
          <?php if (!isset($_SESSION['id'])) {
            echo '<li class="nav-item">
            <a class="nav-link" href="login.php">Login</a>
          </li>';
          }else{
              echo '<li class="nav-item">
                    <a class="nav-link" href="logout.php">logout</a>
                  </li>';
            }
          ?>
          
          
        </ul>
      </div>
    </div>
  </nav>