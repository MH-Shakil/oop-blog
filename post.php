<?php 
session_start();
  include"db/connection.php";
  include"pages/header.php";
  include"pages/nav.php";
  include"classes/user-class.php";

  $post = new post();
  if (isset($_POST['submit']) && $_SERVER['REQUEST_METHOD']=="POST") {
    $post = $post->createPost($_POST,$_FILES);
    if ($post) {
      header("location:index.php");
    }else{
      echo "data not inserted";
    }
  }

 ?>
<div class="card" style="margin: 60px 270px; width: 800px;">
  <div class="card-header" style="background: #45B39D;"><h3 style="padding: 0px;margin: 0px; color: #fff;">Write your post</h3></div>
  <div class="card-body">
      <form action="" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <h3>Title</h3>
                <input class="form-control mb-2" type="text" name="title" style="border-radius: 50px 50px 50px 50px;border: none;" placeholder="Enter a post title">
                <h3>Post</h3>
                <textarea class="form-control" rows="3" name="post"></textarea>
                <input type="file" name="file" class="mt-2">
              </div>
         <button type="submit" class="btn btn-primary" style="width: 100px;" name="submit">Post</button>
      </form>
  </div>
</div>